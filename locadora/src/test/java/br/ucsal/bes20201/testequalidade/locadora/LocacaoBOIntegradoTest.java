package br.ucsal.bes20201.testequalidade.locadora;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.databuilder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.bes20201.testequalidade.locadora.util.ValidadorUtil;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		VeiculoBuilder veiculo = VeiculoBuilder.aVeiculo();
		veiculo.withAno(2019).withValorDiaria(200.00).withSituacao(SituacaoVeiculoEnum.LOCADO);
		veiculos.add(veiculo.build());
		
		VeiculoBuilder veiculo2 = VeiculoBuilder.aVeiculo();
		veiculo2.withAno(2019).withValorDiaria(500.00).withNoSituacao();
		veiculos.add(veiculo2.build());
		
		VeiculoBuilder veiculo3 = VeiculoBuilder.aVeiculo();
		Modelo modelo = new Modelo("Fiat");
		veiculo3.withAno(2012).withValorDiaria(100.00).withModelo(modelo);
		veiculos.add(veiculo3.build());
		
		VeiculoBuilder veiculo4 = VeiculoBuilder.aVeiculo();
		veiculo4.withAno(2012).withValorDiaria(150.00).withNoModelo();
		veiculos.add(veiculo4.build());
		
		VeiculoBuilder veiculo5 = VeiculoBuilder.aVeiculo();
		veiculo5.withAno(2012).withValorDiaria(180.00).withNoPlaca();
		veiculos.add(veiculo5.build());
		
		Assertions.assertEquals(3261.00,LocacaoBO.calcularValorTotalLocacao(veiculos,3));
	}

}
